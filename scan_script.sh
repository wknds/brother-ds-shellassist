#!/bin/bash

SCAN_DESTINATION="."
IMG_PREFIX="out"
IMG_SUFFIX="jpg"
FILENAME="$(date +%Y%m%d-%H%M%S)-scan"

handle_arg () {
	if [[ $1 != "" ]]
	then
		SCAN_DESTINATION=$1
		cd $(realpath $SCAN_DESTINATION)
	fi
}
ask_filename () {
	read -p "Dateiname: " FILENAME_TMP
	if [[ $FILENAME_TMP != "" ]]
	then
		FILENAME="$FILENAME_TMP"
	fi
}

ask_scan_option () {
	read -p "Doppelseitig (2) oder einseitig (1) oder fertig (0)? [1]" SCAN_OPTION
	if [[ $SCAN_OPTION != 2 && $SCAN_OPTION != 1 && $SCAN_OPTION != 0 ]]
	then
		SCAN_OPTION=1
	fi
	return $SCAN_OPTION
}

function merge_scans() {
	img2pdf --output ${FILENAME}.pdf ${IMG_PREFIX}*.${IMG_SUFFIX}
}

function delete_scans() {
	rm ${IMG_PREFIX}*.${IMG_SUFFIX}
}

function apply_ocr() {
	ocrmypdf -l deu --rotate-pages ${FILENAME}.pdf ${FILENAME}.pdf
}

function run_scans() {
	scan_option=99
	scanned_image_counter=1
	while [[ $scan_option != 0 ]]
	do
		ask_scan_option
		scan_option=$?
		if [[ $scan_option -eq 0 ]]
		then
			continue
		elif [[ $scan_option -eq 1 ]]
		then
			scanimage --resolution 300 --AutoDocumentSize=yes --AutoDeskew=yes --format "jpeg" -o "${IMG_PREFIX}${scanned_image_counter}.$IMG_SUFFIX"
			scanned_image_counter=$(($scanned_image_counter+1))
		elif [[ $scan_option -eq 2 ]]
		then
			scanimage --resolution 300 --AutoDocumentSize=yes --AutoDeskew=yes --source "Automatic Document Feeder(left aligned,Duplex)" -b --batch-count=2 --batch-start=$scanned_image_counter --format "jpeg"
			scanned_image_counter=$(($scanned_image_counter+2))
		fi
	done
}

handle_arg "$@"
ask_filename
run_scans
merge_scans
apply_ocr
delete_scans
